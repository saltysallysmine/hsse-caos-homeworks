# HSSE CAOS Homeworks

This is the repository with homeworks on the subject Computer Architecture and Operating Systems.

## Iostream

This is the educational implementation of `std::ostream` and `std::istream` classes using system calls. It can be compiled as a dynamic library or run as program.

For more information go to the `iostream/README.md`.

## Recursive iterator

This is the educational implementation of `std::filesystem::recursive_directory_iterator`.
