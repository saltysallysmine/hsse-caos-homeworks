#include <dirent.h>
#include <stddef.h>

#include "../include/iostream.h"

int main(int argc, char* argv[]) {
  const char* path = (argc > 1) ? argv[1] : ".";
  DIR* dir = opendir(path);

  if (dir == NULL) {
    stdlike::cout << "Error while opening dir " << path << '\n';
    return 1;
  }

  stdlike::cout << "Dir opened\n";

  struct dirent* entry;
  while ((entry = readdir(dir)) != NULL) {
    stdlike::cout << entry->d_name << '\n';
  }

  closedir(dir);
  return 0;
}
