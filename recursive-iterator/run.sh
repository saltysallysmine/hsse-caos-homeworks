#!/bin/bash

INCLUDE=/home/valery/Works/CAOS/Homeworks/recursive-iterator/include/

function build_iostream() {
	cd ../iostream/ && ./run.sh build &&
		cp iostream.h ../recursive-iterator/include/ &&
		cp libiostream.so ../recursive-iterator/include/libiostream.so
}

build_iostream

g++ test/test.cpp -liostream -L./include -fsanitize=leak,address,undefined
LD_LIBRARY_PATH=$INCLUDE ./a.out $1
