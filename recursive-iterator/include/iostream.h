namespace stdlike {

class ostream {
 private:
  // File descriptor of stream to print in
  const int kStreamFD = 1;
  static const int kMaxBufferSize = 1'024;
  const float kFractionalPrecision = 100'000;
  char current_str[30];

  char buffer[kMaxBufferSize];
  int buffer_size = 0;

  // Write error message to the error stream (2)
  void write_error();

  // Write the buffer to the stream
  void write_buffer();

  // Convert integer (short, int, long, long long) to string
  template <typename Integer>
  void int_to_str(Integer number);  // fills current_str

  template <typename Integer>
  ostream& write_integer(Integer number);

  template <typename Fractional>
  ostream& write_fractional(Fractional number);

  void ull_to_hex(unsigned long long number);

 public:
  // points to the first unused buffer character

  // Clear the buffer and display its contents on the screen
  void flush();

  // Put symbol into the buffer and flush it if buffer_size reached
  void put(char c);

  // Write in buffer methods

  ostream& operator<<(char c);

  ostream& operator<<(const char* s);
  ostream& operator<<(char* s);

  ostream& operator<<(const short& n);
  ostream& operator<<(const int& n);
  ostream& operator<<(const long& n);
  ostream& operator<<(const long long& n);
  ostream& operator<<(const unsigned short& n);
  ostream& operator<<(const unsigned int& n);
  ostream& operator<<(const unsigned long& n);
  ostream& operator<<(const unsigned long long& n);

  ostream& operator<<(const float& f);
  ostream& operator<<(const double& f);
  ostream& operator<<(const long double& f);

  ostream& operator<<(const void* v);

  // Constructors and destructors

  ostream(){};

  ostream(int stream_fd) : kStreamFD(stream_fd) {}

  ~ostream();
};

class istream {
 private:
  // File descriptor of stream to print in
  const int kStreamFD = 0;
  static const int kMaxBufferSize = 1'024;
  char current_str[30];
  char buffer[kMaxBufferSize];

  // points to the first unfilled buffer character
  int buffer_size = 0;
  // points to the first unused buffer character
  int buffer_pos = 0;
  /* buffer_pos <= buffer_size. buffer_pos indicates position of symbols that
   * should be written to the targets, buffer_size indicates position of symbols
   * that should be filled in the next read_to_buffer call
   *
   * "cin >> str;" invoked, then:
   *                 buffer_pos                    buffer_size
   *                       |                             |
   *                       v                             v
   * | H | e | l | l | o | , |  | w | o | r | l | d |  | ... | rubbish |
   */

  ostream* linked_output = nullptr;

  // "Is buffer[buffer_pos] equal to <space>, <tab> or <CR>?"
  bool pos_is_empty();

  void read_to_buffer();  // Read to buffer until it encounters a line break,
                          // end of file, or runs out of characters.
  void read_if_need();    // If filled buffer is used, clear it if the whole
                          // buffer filled and call read_to_buffer.
  void goto_not_empty();  // Move buffer_pos to first not empty character.

  // Fill current_str with the buffer content until whitespace character or EOF
  void fill_current_str();

  template <typename Integer>
  istream& read_integer(Integer& number);

  template <typename Fractional>
  istream& read_fractional(Fractional& number);

 public:
  void clear_buffer();  // set buffer_pos = buffer_size = 0

  char peek();  // peek at next character without taking it out
  char get();   // get single character from the buffer

  istream& operator>>(char& symb);

  // Integers

  istream& operator>>(short& number);
  istream& operator>>(int& number);
  istream& operator>>(long& number);
  istream& operator>>(long long& number);
  istream& operator>>(unsigned short& n);
  istream& operator>>(unsigned int& n);
  istream& operator>>(unsigned long& n);
  istream& operator>>(unsigned long long& n);

  // Fractional

  istream& operator>>(float& f);
  istream& operator>>(double& f);
  istream& operator>>(long double& f);

  // Constructors and destructors

  istream(){};

  istream(int stream_fd) : kStreamFD(stream_fd) {}

  istream(ostream* linked_out) : linked_output(linked_out) {}

  ~istream() {}
};

extern ostream cout;
extern istream cin;  // should be initialized with linked_out == cout

}  // namespace stdlike
