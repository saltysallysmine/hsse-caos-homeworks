#include <iostream>

#include "iostream.cpp"

// using namespace stdlike;
using namespace std;

int main() {
  const char* s = "abcde\n";
  cout << s;

  int x;
  cin >> x;

  double d;
  cin >> d;

  cout << d + x << '\n';
  cin >> d;

  cout << 2 * d << ' ' << d + x << '\n';

  char c;
  cin >> c;

  --c;
  cout << c;
  return 0;
}
