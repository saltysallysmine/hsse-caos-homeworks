#!/bin/bash

IOSTREAM_PATH=/home/valery/Works/CAOS/Homeworks/iostream

if [ $# == 0 ]; then
	g++ iostream.cpp tests/test.cpp -fsanitize=leak && ./a.out <tests/in.txt
	exit
fi

case $1 in

build) # only build library without running tests
	g++ -shared -o libiostream.so -fPIC iostream.cpp
  echo "Library was successfully built"
	;;

man) # input tests manually
	g++ iostream.cpp tests/test.cpp -fsanitize=leak,address,undefined && ./a.out
	;;

lib) # build library library and run tests
	g++ -shared -o libiostream.so -fPIC iostream.cpp
	g++ tests/test.cpp -liostream -L.
	LD_LIBRARY_PATH=$IOSTREAM_PATH ./a.out <tests/in.txt
	;;

lector) # build
	g++ -shared -o libiostream.so -fPIC iostream.cpp
	g++ lector_test.cpp -liostream -L.
	LD_LIBRARY_PATH=$IOSTREAM_PATH ./a.out
	;;

*)
	echo "Incorrect parameters were given"
	;;

esac
