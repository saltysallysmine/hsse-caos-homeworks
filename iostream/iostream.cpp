#include "iostream.h"

#include <errno.h>
#include <unistd.h>

// Ostream

stdlike::ostream::~ostream() { flush(); }

// Private methods

void stdlike::ostream::write_error() { ssize_t result = write(2, "Error!", 7); }

void stdlike::ostream::write_buffer() {
  for (int cnt_to_write = buffer_size; cnt_to_write > 0;) {
    ssize_t result =
        write(kStreamFD, buffer + buffer_size - cnt_to_write, buffer_size);

    if (result > 0) {
      cnt_to_write -= result;
      continue;
    }

    write_error();
    break;
  }
}

template <typename Integer>
void stdlike::ostream::int_to_str(Integer number) {
  // find out sign and make number non-negative
  int pos = 0;
  int sign = number >= 0 ? 1 : -1;
  number *= sign;  // now number >= 0

  // find out max ten power less than number
  Integer exp = 1;
  int pow = 0;
  while (exp <= number / 10) {
    exp *= 10;
    ++pow;
  }

  // prepare for calculating abs(number) -> str
  int length = sign == 1 ? pow : pow + 1;
  if (sign == -1) {
    current_str[0] = '-';
    ++pos;
  }

  // main part
  while (exp >= 1) {
    current_str[pos] = static_cast<char>((number / exp) % 10) + '0';
    ++pos;
    exp /= 10;
  }
  current_str[pos] = '\0';
}

template <typename Integer>
stdlike::ostream& stdlike::ostream::write_integer(Integer number) {
  int_to_str(number);
  *this << current_str;
  return *this;
}

template <typename Fractional>
stdlike::ostream& stdlike::ostream::write_fractional(Fractional number) {
  // exponent
  int exp = static_cast<long long>(number);
  *this << exp << ".";

  if (number < 0) {
    number *= -1;
    exp *= -1;
  }  // now f >= exp >= 0

  // mantissa
  float mant = (number - exp) * kFractionalPrecision;
  *this << static_cast<long long>(mant);

  return *this;
}

void stdlike::ostream::ull_to_hex(unsigned long long number) {
  const int kStrSize = 14;

  current_str[0] = '0';
  current_str[1] = 'x';
  current_str[kStrSize] = '\0';

  const char* symbols = "0123456789abcdef";
  int pos = kStrSize - 1;
  while (number > 0) {
    int remainder = number % 16;
    current_str[pos] = symbols[remainder];
    --pos;
    number /= 16;
  }
}

// Public methods

void stdlike::ostream::flush() {
  write_buffer();
  buffer_size = 0;
}

void stdlike::ostream::put(char c) {
  if (buffer_size == kMaxBufferSize) {
    flush();
  }
  buffer[buffer_size] = c;
  ++buffer_size;
}

stdlike::ostream& stdlike::ostream::operator<<(char c) {
  put(c);
  return *this;
}

stdlike::ostream& stdlike::ostream::operator<<(const char* s) {
  for (int size = 0; s[size] != '\0'; ++size) {
    put(s[size]);
  }
  return *this;
}

stdlike::ostream& stdlike::ostream::operator<<(char* s) {
  *this << const_cast<const char*>(s);
  return *this;
}

// Integer

// signed

stdlike::ostream& stdlike::ostream::operator<<(const short& n) {
  return write_integer(n);
}

stdlike::ostream& stdlike::ostream::operator<<(const int& n) {
  return write_integer(n);
}

stdlike::ostream& stdlike::ostream::operator<<(const long& n) {
  return write_integer(n);
}

stdlike::ostream& stdlike::ostream::operator<<(const long long& n) {
  return write_integer(n);
}

// unsigned

stdlike::ostream& stdlike::ostream::operator<<(const unsigned short& n) {
  return write_integer(n);
}

stdlike::ostream& stdlike::ostream::operator<<(const unsigned int& n) {
  return write_integer(n);
}

stdlike::ostream& stdlike::ostream::operator<<(const unsigned long& n) {
  return write_integer(n);
}

stdlike::ostream& stdlike::ostream::operator<<(const unsigned long long& n) {
  return write_integer(n);
}

// Fractional

stdlike::ostream& stdlike::ostream::operator<<(const float& f) {
  return write_fractional(f);
}

stdlike::ostream& stdlike::ostream::operator<<(const double& f) {
  return write_fractional(f);
}

stdlike::ostream& stdlike::ostream::operator<<(const long double& f) {
  return write_fractional(f);
}

// Pointer

stdlike::ostream& stdlike::ostream::operator<<(const void* ptr) {
  ull_to_hex(reinterpret_cast<unsigned long long>(ptr));
  *this << current_str;
  return *this;
}

// Istream

// Private methods

void stdlike::istream::read_to_buffer() {
  if (buffer_size == kMaxBufferSize) {
    // means that the read_to_buffer was called incorrectly
    return;
  }
  if (linked_output != nullptr) {
    linked_output->flush();
  }

  int read_cnt = 0;
  while (buffer_size != kMaxBufferSize && read_cnt != 1) {
    read_cnt =
        read(kStreamFD, buffer + buffer_size, kMaxBufferSize - buffer_size);

    if (read_cnt == -1 || read_cnt == 0) {  // error or EOF
      break;
    }

    buffer_size += read_cnt;                // read_cnt >= 1
    if (buffer[buffer_size - 1] == '\n') {  // end of line
      break;
    }
  }
}

bool stdlike::istream::pos_is_empty() {
  char& c = buffer[buffer_pos];
  return c == ' ' || c == '\t' || c == '\n';
}

void stdlike::istream::read_if_need() {
  if (buffer_pos == kMaxBufferSize) {  // the whole buffer used
    clear_buffer();
  }
  if (buffer_pos == buffer_size) {  // need to read new symbols to buffer
    read_to_buffer();
  }
}

void stdlike::istream::goto_not_empty() {
  read_if_need();
  while (pos_is_empty()) {
    ++buffer_pos;
    read_if_need();
  }
}

void stdlike::istream::fill_current_str() {
  goto_not_empty();  // now pos is not empty

  int str_pos = 0;
  while (!pos_is_empty()) {
    current_str[str_pos] = buffer[buffer_pos];
    ++buffer_pos;
    ++str_pos;
    read_if_need();
  }
  current_str[str_pos] = '\0';
}

template <typename Integer>
stdlike::istream& stdlike::istream::read_integer(Integer& number) {
  fill_current_str();

  bool negative = current_str[0] == '-' ? 1 : 0;
  int str_pos = negative ? 1 : 0;
  number = 0;
  for (; current_str[str_pos] != '\0'; ++str_pos) {
    number *= 10;
    number += static_cast<Integer>(current_str[str_pos] - '0');
  }
  if (negative) {
    number *= -1;
  }

  return *this;
}

template <typename Fractional>
stdlike::istream& stdlike::istream::read_fractional(Fractional& number) {
  fill_current_str();

  bool negative = current_str[0] == '-' ? 1 : 0;
  int str_pos = negative ? 1 : 0;
  number = 0;
  for (; current_str[str_pos] != '.' && current_str[str_pos] != '\0';
       ++str_pos) {
    number *= 10;
    number += static_cast<Fractional>(current_str[str_pos] - '0');
  }

  if (current_str[str_pos] == '\0') {  // if number is integer (123. == 123.0)
    return *this;
  }

  ++str_pos;
  Fractional ten_pow = 0.1;
  for (; current_str[str_pos] != '\0'; ++str_pos) {
    number += static_cast<Fractional>(current_str[str_pos] - '0') * ten_pow;
    ten_pow /= 10;
  }

  if (negative) {
    number *= -1;
  }

  return *this;
}

// Public methods

void stdlike::istream::clear_buffer() {
  buffer_pos = 0;
  buffer_size = 0;
}

char stdlike::istream::peek() {
  read_if_need();
  return buffer[buffer_pos];
}

char stdlike::istream::get() {
  read_if_need();
  return buffer[buffer_pos++];
}

stdlike::istream& stdlike::istream::operator>>(char& symb) {
  goto_not_empty();
  symb = get();
  return *this;
}

// Integer

// signed

stdlike::istream& stdlike::istream::operator>>(short& number) {
  return read_integer(number);
}

stdlike::istream& stdlike::istream::operator>>(int& number) {
  return read_integer(number);
}

stdlike::istream& stdlike::istream::operator>>(long& number) {
  return read_integer(number);
}

stdlike::istream& stdlike::istream::operator>>(long long& number) {
  return read_integer(number);
}

// unsigned

stdlike::istream& stdlike::istream::operator>>(unsigned short& number) {
  return read_integer(number);
}

stdlike::istream& stdlike::istream::operator>>(unsigned int& number) {
  return read_integer(number);
}

stdlike::istream& stdlike::istream::operator>>(unsigned long& number) {
  return read_integer(number);
}

stdlike::istream& stdlike::istream::operator>>(unsigned long long& number) {
  return read_integer(number);
}

// Fractional

stdlike::istream& stdlike::istream::operator>>(float& number) {
  return read_fractional(number);
}

stdlike::istream& stdlike::istream::operator>>(double& number) {
  return read_fractional(number);
}

stdlike::istream& stdlike::istream::operator>>(long double& number) {
  return read_fractional(number);
}

// cin and cout

stdlike::ostream stdlike::cout = stdlike::ostream();
stdlike::istream stdlike::cin = stdlike::istream(&stdlike::cout);
