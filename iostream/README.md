# Iostream

This is the educational implementation of Istream and Ostream objects.

**Completed by Valery Bergman.**

There are `cin` and `cout` objects in `stdlike::` namespace, that you can use.

## Running

Change `IOSTREAM_PATH` in `iostream/run.sh`. Then execute on of the folowing:

```sh
./run.sh        # run tests
./run.sh build  # build library without running tests.
                # `libiostream.so` file will appear in the iostream/
./run.sh man    # input tests manually
./run.sh lib    # build library library and run tests
./run.sh lector # build library and run tests/lector_test.cpp
```
