#include <fcntl.h>
#include <stdio.h>

#include "../iostream.h"

const char* str = "Dododo";
char* dynamic_str = new char[]{"Dododo"};

// Logs

void LogExpected(const char* expected) {
  printf("Expected: %s\n", expected);
  fflush(stdout);
}

void LogTestHead(const char* message, const char* str = "") {
  printf("\n----\n");
  printf("%s %s\n", message, str);
  fflush(stdout);
}

// Tests

// Ostream

void OstreamPutTest1() {
  LogTestHead("Ostream. Put. Test1. Should print");
  LogExpected("c");

  stdlike::cout.put('c');
  stdlike::cout.flush();
}

void OstreamPutTest2() {
  LogTestHead("Ostream. Put. Test2.");
  LogExpected("\"Error!\" in the error stream");

  char non_existent_file[] =
      "/home/valery/Works/CAOS/Homeworks/caos-iostream/tests/non-existent.txt";
  int fd = open(non_existent_file, O_RDONLY);
  stdlike::cout.put(str[0]);
  stdlike::cout.flush();
}

void OstreamStringAndCharTest() {
  LogTestHead("Ostream. String and char test.");
  LogExpected("Dododo Dododo");
  LogExpected("c c");

  char symb = 'c';
  stdlike::cout << str << ' ' << dynamic_str << '\n';
  stdlike::cout << symb << ' ' << symb;
  stdlike::cout.flush();
}

void OstreamBufferOverflowTest() {
  LogTestHead("Ostream. Buffer overflow test.");
  LogExpected("Should print Dododo");

  stdlike::cout << str;
  stdlike::cout.flush();
}

void OstreamIntTest() {
  LogTestHead("Ostream. Integer test.");
  LogExpected("Integers from [-10, 15]");

  for (int i = -10; i <= 15; ++i) {
    stdlike::cout << i << " ";
  }
  stdlike::cout.flush();
}

void OstreamLongLongTest() {
  LogTestHead("Ostream. Long long test.");
  LogExpected("-4'000'000'000 4'000'000'000");

  long long l1 = -4'000'000'000;
  long long l2 = 4'000'000'000;
  stdlike::cout << l1 << " " << l2;
  stdlike::cout.flush();
}

void OstreamAllIntegersTest() {
  LogTestHead("Ostream. Test all integers");
  LogExpected("1 1 \\n 2 2 \\n 3 3 \\n 4 4 \\n");

  short a = 1;
  unsigned short ua = 1;
  int b = 2;
  unsigned int ub = 2;
  long c = 3;
  unsigned long uc = 3;
  long long d = 4;
  unsigned long long ud = 4;
  stdlike::cout << a << " " << ua << "\n";
  stdlike::cout << b << " " << ub << "\n";
  stdlike::cout << c << " " << uc << "\n";
  stdlike::cout << d << " " << ud << "\n";
  stdlike::cout.flush();
}

void OstreamFloatTest() {
  LogTestHead("Ostream. Float test.");
  LogExpected("-9876.54321 123.456 0.0 12.0 (with small error)");

  float f1 = -9876.54321;
  float f2 = 123.456;
  float f3 = 0.0;
  float f4 = 12.0;
  stdlike::cout << f1 << " " << f2 << " " << f3 << " " << f4;
  stdlike::cout.flush();
}

void OstreamAllFractionalTest() {
  LogTestHead("Ostream. All fractional test.");
  LogExpected("-9876.54321 123.456 0.1 (with small error)");

  float f1 = -9876.54321;
  double f2 = 123.456;
  long double f3 = 0.1;
  stdlike::cout << f1 << " " << f2 << " " << f3;
  stdlike::cout.flush();
}

void OstreamPointerTest() {
  LogTestHead("Ostream. Pointers test");

  int a[] = {1, 2, 3};
  int* pa[] = {&a[0], &a[1], &a[2]};

  char expected[50];
  sprintf(expected, "%p %p %p", pa[0], pa[1], pa[2]);
  LogExpected(expected);

  stdlike::cout << pa[0] << " " << pa[1] << " " << pa[2] << "\n";
  stdlike::cout.flush();
}

// Istream

void IstreamPeekAndGetTest() {
  LogTestHead("Istream. Peek and get test. Input: number with 3-4 chars");
  LogExpected("1, 1, 1, 2, 3");

  stdlike::cout << stdlike::cin.peek() << ", ";
  stdlike::cout << stdlike::cin.peek() << ", ";
  stdlike::cout << stdlike::cin.get() << ", ";
  stdlike::cout << stdlike::cin.get() << ", ";
  stdlike::cout << stdlike::cin.get();

  stdlike::cout.flush();
}

void IstreamCharTest() {
  LogTestHead("Istream. Char test. Input: 1 number (4-5 digits)");
  LogExpected("1 2");

  char c1;
  char c2;
  stdlike::cin >> c1 >> c2;
  stdlike::cout << c1 << " " << c2;

  stdlike::cout.flush();
}

void IstreamIntegersTest() {
  LogTestHead("Istream. Integers test");
  LogExpected("10 22 34 56. Input: 4 integers from 10 to 100");

  short a;
  int b;
  long c;
  long long d;
  stdlike::cin >> a >> b >> c >> d;
  stdlike::cout << a << " " << b << " " << c << " " << d;

  stdlike::cout.flush();
}

void IstreamFractionalsTest() {
  LogTestHead("Istream. Fractionals test");
  LogExpected("1.0 2.2 0.34. Input: 4 fractionals");

  float f;
  double d;
  long double ld;
  stdlike::cin >> f >> d >> ld;
  stdlike::cout << f << " " << d << " " << ld;

  stdlike::cout.flush();
}

// Manual istream

void IstreamCoutFlushTest() {
  LogTestHead("Istream. Cout flush test");
  LogExpected("\"Dododo\\n\", than input from from STDIN, than \"1234567890\"");

  long long int str;
  stdlike::cout << "Dododo\n";
  stdlike::cin >> str;
  stdlike::cout << str << '\n';

  stdlike::cout.flush();
  stdlike::cin.clear_buffer();
}

// Main

void OstreamTest() {
  printf("Ostream tests.");
  OstreamPutTest1();            // put symbol
  OstreamPutTest2();            // non existent file
  OstreamStringAndCharTest();   // strings and chars
  OstreamBufferOverflowTest();  // too long string
  OstreamIntTest();             // int
  OstreamLongLongTest();        // long long
  OstreamAllIntegersTest();     // short ... ll, unsigned short ... ull
  OstreamFloatTest();           // float
  OstreamAllFractionalTest();   // float, double, long double
  OstreamPointerTest();         // pointers
}

void IstreamTest() {
  printf("Istream tests.");
  // Order of tests is important!
  IstreamPeekAndGetTest();   // 1234
  IstreamCharTest();         // 1 2 34
  IstreamIntegersTest();     // 10 22 34 56
  IstreamFractionalsTest();  // 1.0 2.2 0.34

  // Manual tests
  // IstreamCoutFlushTest();  // execute `./run man` to run without in.txt
}

int main() {
  OstreamTest();
  IstreamTest();  // also uses cout
  return 0;
}
